package com.tonyhendra.tokosayur;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by tonyh on 29-May-18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMessagingService";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //super.onMessageReceived(remoteMessage);

        //It is Optional
        Log.d("TAG", "From : " + remoteMessage.getFrom());
        Log.d("TAG", "Notification Message Body : " + remoteMessage.getNotification().getBody());

        sendNotification(remoteMessage.getFrom(),remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
    }

    /**
     * this method is only generating push notification
     */
    private void sendNotification(String topic, String title, String messageBody){
        Intent intent = new Intent();
        if(topic.equals("customer")){
            intent = new Intent(this, MainActivity.class);
        }else if(topic.equals("driver")){
            intent = new Intent(this, DriverMainActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //intent.putExtra("tabPosition", 2);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0,notificationBuilder.build());
    }
}