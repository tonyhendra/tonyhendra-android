package com.tonyhendra.tokosayur;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.tonyhendra.tokosayur.adapter.RutePickupAdapter;
import com.tonyhendra.tokosayur.model.RuteModel;
import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverRutePickupActivity extends AppCompatActivity {
    SharedPreferences pref;
    String id, token;
    Context mContext;
    BaseApiService mApiService;
    RecyclerView recyclerView;
    ArrayList<RuteModel> ruteModel;
    RutePickupAdapter adapter;
    ProgressDialog pDialog;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_rute_pickup);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        pref = getSharedPreferences("data", Context.MODE_PRIVATE);
        id = pref.getString("id","");
        token = pref.getString("token","");

        mContext = getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setAutoMeasureEnabled(false);
        recyclerView.setLayoutManager(llm);
        getRutePickup(token, id);

    }

    public void getRutePickup(String token, String id_driver){
        pDialog = new ProgressDialog(this);
        pDialog.setMax(30);
        pDialog.show();
        mApiService.getRutePickup(token, id_driver).enqueue(new Callback<ArrayList<RuteModel>>() {
            @Override
            public void onResponse(Call<ArrayList<RuteModel>> call, Response<ArrayList<RuteModel>> response) {
                pDialog.dismiss();
                ArrayList<RuteModel> ruteModels = response.body();

                adapter = new RutePickupAdapter(ruteModels, mContext);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<ArrayList<RuteModel>> call, Throwable t) {
                pDialog.dismiss();
            }
        });
    }
}
