package com.tonyhendra.tokosayur;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.tonyhendra.tokosayur.adapter.RecyclerViewDataAdapter;
import com.tonyhendra.tokosayur.adapter.SectionListDataAdapter;
import com.tonyhendra.tokosayur.model.SectionDataModel;
import com.tonyhendra.tokosayur.model.SingleItemModel;
import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductActivity extends AppCompatActivity {

    private ProgressDialog pDialog;

    RecyclerView my_recycler_view;
    BaseApiService mApiService;
    GridLayoutManager gridLayoutManager;

    SectionListDataAdapter adapter;

    ArrayList<SingleItemModel> allSampleData = new ArrayList<>();
    SharedPreferences pref;
    String token, tittle;
    Toolbar toolbar;
    TextView tv_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        pref = getApplicationContext().getSharedPreferences("data", Context.MODE_PRIVATE);
        token = pref.getString("token","");
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper

        tv_count = (TextView)findViewById(R.id.tv_count);


        String id_kategori;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                id_kategori= null;
            } else {
                id_kategori= extras.getString("id_kategori");
            }
        } else {
            id_kategori = (String) savedInstanceState.getSerializable("STRING_I_NEED");
        }

        toolbar = (Toolbar)findViewById(R.id.toolbar_product);
        if(id_kategori==null){
            toolbar.setTitle("Product");
        }else{
            loadKategori(id_kategori);
        }


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        my_recycler_view = (RecyclerView)findViewById(R.id.my_recycler_view);
        my_recycler_view.setHasFixedSize(true);
        gridLayoutManager = new GridLayoutManager(getApplicationContext(),2);
        my_recycler_view.setLayoutManager(gridLayoutManager);
        loadJSON(token,id_kategori);

    }

    private void loadJSON(String token, final String id_kategori){
        pDialog = new ProgressDialog(ProductActivity.this);
        pDialog.setMax(300);
        pDialog.setMessage("Harap Tunggu...");
        pDialog.show();
        mApiService.getProductJSON(token,id_kategori,0,"").enqueue(new Callback<ArrayList<SingleItemModel>>() {
            @Override
            public void onResponse(Call<ArrayList<SingleItemModel>> call, Response<ArrayList<SingleItemModel>> response) {
                pDialog.dismiss();
                ArrayList<SingleItemModel> singleItem = response.body();
                adapter = new SectionListDataAdapter(getApplicationContext(), singleItem);
                my_recycler_view.setAdapter(adapter);

                int count = singleItem.size();
                tv_count.setText(count+" Produk");
            }

            @Override
            public void onFailure(Call<ArrayList<SingleItemModel>> call, Throwable t) {
                pDialog.dismiss();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void loadKategori(final String id_kategori){
        mApiService.getKategori().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){

                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonRESULTS.getJSONArray("data");

                        for(int i=0; i<jsonArray.length();i++){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            if(id_kategori.equals(jsonObject.getString("id_kategori"))){
                                toolbar.setTitle(jsonObject.getString("nama_kategori"));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}


