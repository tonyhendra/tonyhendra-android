package com.tonyhendra.tokosayur;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailOrderActivity extends AppCompatActivity {
    Context mContext;
    BaseApiService mApiService;
    SharedPreferences pref;
    Toolbar toolbar;
    ProgressDialog pDialog;
    Button btnKonfirmasi;
    TextView tvTanggal, tvStatus, tvAlamat, tvNamaProduk, tvJumlah, tvTotalHarga, tvTotalBayar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvAlamat = (TextView) findViewById(R.id.tvAlamat);
        tvStatus = (TextView)findViewById(R.id.tvStatus);
        tvNamaProduk = (TextView)findViewById(R.id.tvNamaProduk);
        tvJumlah = (TextView)findViewById(R.id.tvJumlah);
        tvTanggal = (TextView)findViewById(R.id.tvTanggal);
        tvTotalHarga = (TextView)findViewById(R.id.tvTotalHarga);
        tvTotalBayar = (TextView) findViewById(R.id.tvTotalBayar);
        btnKonfirmasi = (Button)findViewById(R.id.btnKonfirmasi);


        pref = this.getSharedPreferences("data", Context.MODE_PRIVATE);
        String token = pref.getString("token","");
        final String id_order, status;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                id_order= null;
                status = null;
            } else {
                id_order= extras.getString("id_order");
                status = extras.getString("status");
            }
        } else {
            id_order = (String) savedInstanceState.getSerializable("STRING_I_NEED");
            status = (String) savedInstanceState.getSerializable("STRING_I_NEED");
        }
        mContext = this.getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper

        getDetailOrder(token, id_order);
        if(status.equals("Menunggu Pembayaran")){
            //nothing
        }else if(status!="Menunggu Pembayaran"){
            btnKonfirmasi.setEnabled(false);
            btnKonfirmasi.setBackgroundColor(Color.parseColor("#cccccc"));
        }
        btnKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DetailOrderActivity.this, KonfirmasiPembayaranActivity.class);
                i.putExtra("id_order", id_order);
                startActivity(i);
            }
        });
    }

    private void getDetailOrder(String token, String id_order){
        pDialog = new ProgressDialog(this);
        pDialog.setMax(30);
        pDialog.setMessage("Harap Tunggu...");
        pDialog.show();
        mApiService.getDetailOrder(token,id_order).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    pDialog.dismiss();
                    try{
                        //JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        tvStatus.setText(jsonObject.getString("status"));
                        tvTanggal.setText(jsonObject.getString("tanggal"));
                        tvAlamat.setText(jsonObject.getString("alamat"));
                        tvNamaProduk.setText(jsonObject.getString("nama"));
                        tvJumlah.setText(jsonObject.getString("jumlah"));
                        tvTotalHarga.setText(jsonObject.getString("total_harga"));
                        tvTotalBayar.setText(jsonObject.getString("total_harga"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{
                    pDialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pDialog.dismiss();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
