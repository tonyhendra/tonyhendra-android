package com.tonyhendra.tokosayur.utils;

import com.tonyhendra.tokosayur.model.NotifikasiModel;
import com.tonyhendra.tokosayur.model.OrderItemModel;
import com.tonyhendra.tokosayur.model.RuteDetailModel;
import com.tonyhendra.tokosayur.model.RuteModel;
import com.tonyhendra.tokosayur.model.SingleItemModel;

import java.util.ArrayList;

import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by tonyhendra on 22/11/2017.
 */

public interface BaseApiService {
    // Fungsi ini untuk memanggil API http://10.0.2.2/mahasiswa/login.php
    @FormUrlEncoded
    @POST("auth/login")
    Call<ResponseBody> loginRequest(@Field("username") String username,
                                    @Field("password") String password);

    // Fungsi ini untuk memanggil API http://10.0.2.2/mahasiswa/register.php
    @FormUrlEncoded
    @POST("auth/register")
    Call<ResponseBody> registerRequest(@Field("username") String username,
                                       @Field("email") String email,
                                       @Field("password") String password,
                                       @Field("nama") String nama,
                                       @Field("tanggal_lahir") String tanggl_lahir,
                                       @Field("jenis_kelamin") String jenis_kelamin,
                                       @Field("no_hp") String no_hp,
                                       @Field("role") Integer role);
    @GET("auth/cek")
    Call<ResponseBody> cekUsername(@Query("username") String username);

    @GET("kategori")
    Call<ResponseBody> getKategori();

    @FormUrlEncoded
    @POST("auth/update")
    Call<ResponseBody> updateProfile(@Field("username") String username,
                                     @Field("email") String email,
                                     @Field("password") String password,
                                     @Field("nama") String nama,
                                     @Field("tanggal_lahir") String tanggl_lahir,
                                     @Field("jenis_kelamin") String jenis_kelamin,
                                     @Field("no_hp") String no_hp
                                     );

    @GET("user")
    Call<ResponseBody> getProfile(@Query("access-token") String token,
                                  @Query("username") String username);

//    @GET("product")
//    Call<ArrayList<SingleItemModel>> getProductJSON(@Query("access-token") String token);

    @GET("notifikasi")
    Call<ArrayList<NotifikasiModel>> getNotifikasi(@Query("access-token") String token,
                                                   @Query("id_customer") String id_customer);

    @FormUrlEncoded
    @POST("cart/add")
    Call<ResponseBody> addCart(@Field("id_product") Integer id_product,
                               @Field("username") String username,
                               @Field("jumlah") Integer jumlah);

    @GET("product")
    Call<ArrayList<SingleItemModel>> getProductJSON(@Query("access-token") String token,
                                                    @Query("id_kategori") String id_kategori,
                                                    @Query("stock") Integer stock,
                                                    @Query("limit") String limit);

    @GET("product")
    Call<ArrayList<SingleItemModel>> getDetailProductJSON(@Query("access-token") String token,
                                                          @Query("id_product") String id_product);

    @GET("orders")
    Call<ArrayList<OrderItemModel>> getOrders(@Query("access-token") String token,
                                              @Query("id_customer") String id_customer);

    @GET("orders/detail-order")
    Call<ResponseBody> getDetailOrder(@Query("access-token") String token,
                                      @Query("id_order") String id_order);

    @FormUrlEncoded
    @POST("orders/create")
    Call<ResponseBody> createOrder(@Query("access-token") String token,
                                   @Field("id_customer") Integer id_customer,
                                   @Field("alamat") String alamat,
                                   @Field("lat") Double lat,
                                   @Field("lng") Double lng,
                                   @Field("total_berat") Double total_berat,
                                   @Field("total_harga") Double total_harga,
                                   @Field("id_product") Integer id_product,
                                   @Field("jumlah") Integer jumlah
                                   );

    @GET("rute-pickup")
    Call<ArrayList<RuteModel>> getRutePickup(@Query("access-token") String token,
                                             @Query("id_driver") String id_driver);

    @GET("rute-delivery")
    Call<ArrayList<RuteModel>> getRuteDelivery(@Query("access-token") String token,
                                               @Query("id_driver") String id_driver);

    @GET("{path}/detail")
    Call<ArrayList<RuteDetailModel>> getRuteDetail(@Path(value = "path", encoded = true) String path,
                                                   @Query("access-token") String token,
                                                   @Query("id_driver") String id_driver,
                                                   @Query("id_rute") String id_rute);
    @GET("{path}/direction")
    Call<ResponseBody> getRuteDirection(@Path(value = "path", encoded = true) String path,
                                                   @Query("access-token") String token,
                                                   @Query("id_rute") String id_rute);

    @GET("depot")
    Call<ResponseBody> getDepot();

    @FormUrlEncoded
    @POST("{path}/konfirmasi")
    Call<ResponseBody> konfirmasi(@Path(value = "path", encoded = true) String path,
                                  @Query("access-token") String token,
                                  @Field("id_ruet") String id_rute
                                  );

    @GET("job")
    Call<ResponseBody> getJobCount(@Query("access-token") String token,
                                   @Query("id_driver") String id_driver);

    @FormUrlEncoded
    @POST("konfirmasi-pembayaran")
    Call<ResponseBody> konfirmasiPembayaran(@Field("id_order") int id_order,
                                            @Field("nama") String nama,
                                            @Field("no_rek") String no_rek,
                                            @Field("bank") String bank,
                                            @Field("nominal") Double nominal,
                                            @Field("tujuan") String tujuan);


}
