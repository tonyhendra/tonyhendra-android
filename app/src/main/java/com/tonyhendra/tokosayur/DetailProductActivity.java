package com.tonyhendra.tokosayur;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tonyhendra.tokosayur.adapter.RecyclerViewDataAdapter;
import com.tonyhendra.tokosayur.model.SectionDataModel;
import com.tonyhendra.tokosayur.model.SingleItemModel;
import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailProductActivity extends AppCompatActivity {
    private CollapsingToolbarLayout collapsingToolbarLayout = null;
    Context mContext;
    BaseApiService mApiService;
    SharedPreferences pref;
    Toolbar toolbar;
    ProgressDialog pDialog;
    Button btnBuy;

    TextView txt_harga,txt_terjual,txt_stock,txt_deskripsi;
    ImageView img_product;
    private String nama, token, id_product, urlImg, berat, harga;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);


        pref = this.getSharedPreferences("data", Context.MODE_PRIVATE);
        String token = pref.getString("token","");
        final String username = pref.getString("username","");
        mContext = this.getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper

        txt_harga = findViewById(R.id.txt_harga);
        txt_terjual = findViewById(R.id.txt_terjual);
        txt_stock = findViewById(R.id.txt_stock);
        txt_deskripsi = findViewById(R.id.txt_deskripsi);
        btnBuy = findViewById(R.id.btnBuy);

        img_product = findViewById(R.id.img_product);

        final String id_product;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                id_product= null;
            } else {
                id_product= extras.getString("id_product");
            }
        } else {
            id_product = (String) savedInstanceState.getSerializable("STRING_I_NEED");
        }
        txt_deskripsi.setText(id_product);

        loadJSON(token, id_product);

        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DetailProductActivity.this,CheckoutActivity.class);
                i.putExtra("id_product", id_product);
                i.putExtra("urlImg", urlImg);
                i.putExtra("berat", berat);
                i.putExtra("harga", harga);
                i.putExtra("nama", nama);
                startActivity(i);

                //addCart(Integer.parseInt(id_product), username,  1);
            }
        });
    }

    private void dynamicToolbarColor(){
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.default_image);
        Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(R.attr.colorPrimary));
                collapsingToolbarLayout.setStatusBarScrimColor(palette.getMutedColor(R.attr.colorPrimaryDark));

            }
        });
    }

    private void loadJSON(String token, String id_product){
        pDialog = new ProgressDialog(DetailProductActivity.this);
        pDialog.setMax(30);
        pDialog.setMessage("Harap Tunggu...");
        pDialog.show();
        mApiService.getDetailProductJSON(token,id_product).enqueue(new Callback<ArrayList<SingleItemModel>>() {
            @Override
            public void onResponse(Call<ArrayList<SingleItemModel>> call, Response<ArrayList<SingleItemModel>> response) {
                pDialog.dismiss();
                ArrayList<SingleItemModel> singleItem = response.body();
                loadData(singleItem);
            }

            @Override
            public void onFailure(Call<ArrayList<SingleItemModel>> call, Throwable t) {
                pDialog.dismiss();
            }
        });

    }

    private void addCart(Integer id_product, String username, Integer jumlah){
        pDialog = new ProgressDialog(DetailProductActivity.this);
        pDialog.setMax(30);
        pDialog.setMessage("Harap Tunggu...");
        pDialog.show();
        mApiService.addCart(id_product,username,jumlah).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pDialog.dismiss();
                if (response.isSuccessful()){
                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("status").equals("success")){
                            //Uncomment the below code to Set the message and title from the strings.xml file
                            //builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);

                            AlertDialog.Builder builder = new AlertDialog.Builder(DetailProductActivity.this);

                            //Setting message manually and performing action on button click
                            builder.setMessage("Berhasil menambahkan produk ke keranjang belanja")
                                    .setCancelable(false)
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                        }
                                    })
                                    .setNegativeButton("Go to Cart", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //  Action for 'NO' Button
                                            dialog.cancel();
//                                            FragmentNotifikasi fragmentCart = new FragmentNotifikasi();
//                                            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                                            fragmentTransaction.replace(R.id., fragmentCart);
//                                            fragmentTransaction.addToBackStack(null);
//                                            fragmentTransaction.commit();

                                        }
                                    });

                            //Creating dialog box
                            AlertDialog alert = builder.create();
                            //Setting the title manually
                            //alert.setTitle("AlertDialogExample");
                            alert.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void loadData(ArrayList<SingleItemModel> item) {

        for (SingleItemModel data : item) {
            //getSupportActionBar().setTitle(data.getNama());
            collapsingToolbarLayout.setTitle(data.getNama());
            txt_harga.setText("Rp "+data.getHarga()+",-");
            txt_stock.setText(data.getStock());
            txt_terjual.setText(data.getTerjual());
            txt_deskripsi.setText(data.getDeskripsi());
            Picasso.with(mContext)
                    .load("http://192.168.43.25//ta/backend/web/uploads/"+data.getImage())
                    .placeholder(R.drawable.default_image)   // optional
                    .error(R.drawable.default_image)      // optional
                    .resize(300, 300)// optional
                    .into(img_product);
            urlImg = "http://192.168.43.25/ta/backend/web/uploads/"+data.getImage();
            berat = data.getBerat();
            harga = data.getHarga();
            nama = data.getNama();
            if(Integer.parseInt(data.getStock())==0){
                btnBuy.setEnabled(false);
                btnBuy.setBackgroundColor(Color.GRAY);
            }
        }

    }





}
