package com.tonyhendra.tokosayur;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.tonyhendra.tokosayur.R;
import com.tonyhendra.tokosayur.adapter.SliderAdapter;
import com.tonyhendra.tokosayur.model.SectionDataModel;
import com.tonyhendra.tokosayur.model.SingleItemModel;
import com.tonyhendra.tokosayur.adapter.RecyclerViewDataAdapter;
import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.JSONResponse;
import com.tonyhendra.tokosayur.utils.UtilsApi;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by tonyhendra on 26/11/2017.
 */

public class FragmentHome extends Fragment {
    private Toolbar toolbar;
    private ProgressDialog pDialog;
    private AsyncTask myTask;

    Context mContext;
    BaseApiService mApiService;
    SharedPreferences pref;

    ArrayList<SectionDataModel> allSampleData;
    RecyclerViewDataAdapter adapter;
    RecyclerView my_recycler_view;

    ViewPager viewPager;
    TabLayout indicator;

    List<String> image;
    List<String> colorName;

    String token;

    SwipeRefreshLayout swLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        allSampleData = new ArrayList<SectionDataModel>();
        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        //toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        swLayout = (SwipeRefreshLayout) view.findViewById(R.id.swlayout);
        //Mengeset properti warna yang berputar pada SwipeRefreshLayout
        swLayout.setColorSchemeResources(R.color.myColor4,R.color.myColor6);

        pref = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        token = pref.getString("token","");
        mContext = getActivity().getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper

        String role = pref.getString("role","");

//        if(role!="3"){
//            Intent i = new Intent(mContext, Main2Activity.class);
//            startActivity(i);
//        }

        viewPager=(ViewPager)view.findViewById(R.id.viewPager);
        indicator=(TabLayout)view.findViewById(R.id.indicator);

        image = new ArrayList<>();
        image.add("http://kvsmt.com/wp-content/uploads/2016/10/front-slider-fresh-vegetables.jpg");
        image.add("http://kvsmt.com/wp-content/uploads/2016/10/front-slider-fresh-vegetables.jpg");
        image.add("http://kvsmt.com/wp-content/uploads/2016/10/front-slider-fresh-vegetables.jpg");

        viewPager.setAdapter(new SliderAdapter(mContext, image));
        indicator.setupWithViewPager(viewPager, true);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMax(30);
        pDialog.setMessage("Harap Tunggu...");
        pDialog.show();

        my_recycler_view = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        my_recycler_view.setHasFixedSize(true);
        my_recycler_view.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        loadKategori(token);

        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // Handler untuk menjalankan jeda selama 5 detik
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {
                        allSampleData.clear();
                        // Berhenti berputar/refreshing
                        swLayout.setRefreshing(false);
                        loadKategori(token);

                    }
                }, 5000);
            }
        });

        return view;

    }

    private void initComponents(View view, String token){


    }

    private void loadKategori(final String token){
        mApiService.getKategori().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){

                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonRESULTS.getJSONArray("data");
                        for(int i=0; i<jsonArray.length();i++){
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            loadJSON(token,jsonObject.getString("id_kategori"), jsonObject.getString("nama_kategori"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void loadJSON(String token, final String id_kategori, final String tittle ){
        mApiService.getProductJSON(token,id_kategori,0,"").enqueue(new Callback<ArrayList<SingleItemModel>>() {
            @Override
            public void onResponse(Call<ArrayList<SingleItemModel>> call, Response<ArrayList<SingleItemModel>> response) {
                SectionDataModel dm = new SectionDataModel();

                ArrayList<SingleItemModel> singleItem = response.body();

                dm.setHeaderTitle(tittle);
                dm.setId_kategori(id_kategori);
                pDialog.dismiss();

                dm.setAllItemsInSection(singleItem);
                allSampleData.add(dm);

                adapter = new RecyclerViewDataAdapter(getActivity(), allSampleData);
                my_recycler_view.setAdapter(adapter);

                }

            @Override
            public void onFailure(Call<ArrayList<SingleItemModel>> call, Throwable t) {
                pDialog.dismiss();
            }
        });
    }

    private class SliderTimer extends TimerTask {
        Activity activity = FragmentHome.this.getActivity();
        @Override
        public void run() {
           activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < image.size() - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }


}
