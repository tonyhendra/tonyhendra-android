package com.tonyhendra.tokosayur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class InfoPembayaranActivity extends AppCompatActivity {
    TextView tvNamaProduk, tvJumlah, tvHarga, tvTotalHarga;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_pembayaran);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Informasi Pembayaran");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvNamaProduk = (TextView)findViewById(R.id.tvNamaProduk);
        tvHarga = (TextView)findViewById(R.id.tvHarga);
        tvJumlah = (TextView)findViewById(R.id.tvJumlah);
        tvTotalHarga = (TextView)findViewById(R.id.tvTotalHarga);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String nama = extras.getString("nama");
            Integer jumlah = extras.getInt("jumlah");
            String harga = extras.getString("harga");
            Double total_harga = extras.getDouble("total_harga");
            //The key argument here must match that used in the other activity
            tvNamaProduk.setText(nama);
            tvJumlah.setText(jumlah.toString());
            tvHarga.setText(harga);
            tvTotalHarga.setText("Total : "+total_harga.toString());

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(InfoPembayaranActivity.this, MainActivity.class);
        startActivity(i);
    }
}
