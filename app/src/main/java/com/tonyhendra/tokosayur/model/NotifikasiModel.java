package com.tonyhendra.tokosayur.model;

/**
 * Created by tonyh on 29-May-18.
 */

public class NotifikasiModel {
    private String id_notifikasi, id_order, id_customer, pesan, tanggal;

    public NotifikasiModel() {
    }

    public NotifikasiModel(String id_notifikasi, String id_order, String id_customer) {
        this.id_notifikasi = id_notifikasi;
        this.id_order = id_order;
        this.id_customer = id_customer;
    }

    public String getId_notifikasi() {
        return id_notifikasi;
    }

    public void setId_notifikasi(String id_notifikasi) {
        this.id_notifikasi = id_notifikasi;
    }

    public String getId_order() {
        return id_order;
    }

    public void setId_order(String id_order) {
        this.id_order = id_order;
    }

    public String getId_customer() {
        return id_customer;
    }

    public void setId_customer(String id_customer) {
        this.id_customer = id_customer;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
