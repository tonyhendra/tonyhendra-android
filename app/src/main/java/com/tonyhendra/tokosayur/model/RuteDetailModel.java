package com.tonyhendra.tokosayur.model;

/**
 * Created by tonyh on 22-May-18.
 */

public class RuteDetailModel {
    private String id, alamat, nama, product, jumlah;

    public RuteDetailModel() {
    }

    public RuteDetailModel(String id, String alamat) {
        this.id = id;
        this.alamat = alamat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }
}

