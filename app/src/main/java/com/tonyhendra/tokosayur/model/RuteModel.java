package com.tonyhendra.tokosayur.model;

/**
 * Created by tonyh on 15-May-18.
 */

public class RuteModel {
    private String id_rute, id_kendaraan, id_driver, berat, rute, total_jarak, tanggal, status;

    public RuteModel() {
    }

    public RuteModel(String id_rute, String id_kendaraan, String id_driver) {
        this.id_rute = id_rute;
        this.id_kendaraan = id_kendaraan;
        this.id_driver = id_driver;
    }

    public String getId_rute() {
        return id_rute;
    }

    public void setId_rute(String id_rute) {
        this.id_rute = id_rute;
    }

    public String getId_kendaraan() {
        return id_kendaraan;
    }

    public void setId_kendaraan(String id_kendaraan) {
        this.id_kendaraan = id_kendaraan;
    }

    public String getId_driver() {
        return id_driver;
    }

    public void setId_driver(String id_driver) {
        this.id_driver = id_driver;
    }

    public String getBerat() {
        return berat;
    }

    public void setBerat(String berat) {
        this.berat = berat;
    }

    public String getRute() {
        return rute;
    }

    public void setRute(String rute) {
        this.rute = rute;
    }

    public String getTotal_jarak() {
        return total_jarak;
    }

    public void setTotal_jarak(String total_jarak) {
        this.total_jarak = total_jarak;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
