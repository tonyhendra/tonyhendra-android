package com.tonyhendra.tokosayur.model;

/**
 * Created by tonyhendra on 07/12/2017.
 */

public class SingleItemModel {
    private String stock;
    private String id_product;
    private String nama;
    private String deskripsi;
    private String description;
    private String harga;
    private String berat;
    private String image;
    private String terjual;



    public String getId_product() {
        return id_product;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public SingleItemModel() {
    }

    public SingleItemModel(String nama, String harga) {
        this.nama = nama;
        this.harga = harga;

    }


    public String getHarga() {
        return harga;
    }

    public void setUrl(float url) {
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public String getTerjual() {
        return terjual;
    }

    public void setTerjual(String terjual) {
        this.terjual = terjual;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBerat() {
        return berat;
    }

    public void setBerat(String berat) {
        this.berat = berat;
    }
}
