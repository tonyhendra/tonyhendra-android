package com.tonyhendra.tokosayur.model;

/**
 * Created by tonyhendra on 14/04/2018.
 */

public class CartItemModel {
    private String id_cart;
    private String username;
    private String id_product;
    private String nama;
    private String harga;
    private String image;
    private String jumlah;

    public CartItemModel() {
    }

    public CartItemModel(String nama, String harga) {
        this.nama = nama;
        this.harga = harga;

    }

    public String getId_cart() {
        return id_cart;
    }

    public void setId_cart(String id_cart) {
        this.id_cart = id_cart;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId_product() {
        return id_product;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }
}