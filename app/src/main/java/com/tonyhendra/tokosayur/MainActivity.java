package com.tonyhendra.tokosayur;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.tonyhendra.tokosayur.R;
import com.tonyhendra.tokosayur.adapter.MainAdapter;

public class MainActivity extends AppCompatActivity {

    SharedPreferences pref;
    Toolbar toolbar;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FirebaseMessaging.getInstance().subscribeToTopic("customer");

        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabLayout);
        final MainAdapter mainAdapter = new MainAdapter(getSupportFragmentManager());
        viewPager = (ViewPager)findViewById(R.id.viewpager);
        pref = getSharedPreferences("data", Context.MODE_PRIVATE);

        viewPager.setAdapter(mainAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(4);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                toolbarTittle(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            Integer tabPos = extras.getInt("tabPosition");
            //The key argument here must match that used in the other activity
            viewPager.setCurrentItem(tabPos);
            toolbarTittle(tabPos);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the main_menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.logout:
                //your action
                logOut();
                break;
            case R.id.exit:
                //your action
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void logOut(){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("token","");
        editor.putString("username","");
        editor.putInt("login",0);
        editor.clear();
        editor.commit();

        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void toolbarTittle(Integer pos){
        if(pos==0){
            toolbar.setTitle("Tokosayur");
        }else if(pos==1){
            toolbar.setTitle("Orders");
        }else if(pos==2){
            toolbar.setTitle("Notifikasi");
        }else if(pos==3){
            toolbar.setTitle("Profile");
        }else {
            toolbar.setTitle("Tokosayur");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        try {
            //on click notification
            Bundle extras = intent.getExtras();
            Integer tabPos = extras.getInt("tabPosition");
            //The key argument here must match that used in the other activity
            viewPager.setCurrentItem(tabPos);
            toolbarTittle(tabPos);
        } catch (Exception e) {
            Log.e("onclick", "Exception onclick" + e);
        }
    }


}
