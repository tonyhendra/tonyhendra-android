package com.tonyhendra.tokosayur;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tonyhendra.tokosayur.adapter.NotifikasiAdapter;
import com.tonyhendra.tokosayur.model.NotifikasiModel;
import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tonyh on 08-Apr-18.
 */

public class FragmentNotifikasi extends Fragment {
    RecyclerView my_recycler_view;
    private ProgressDialog pDialog;
    Context mContext;
    BaseApiService mApiService;
    NotifikasiAdapter adapter;
    SharedPreferences pref;
    String id, token;
    SwipeRefreshLayout swLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notifikasi, null, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);

        swLayout = (SwipeRefreshLayout) view.findViewById(R.id.swlayout);
        // Mengeset properti warna yang berputar pada SwipeRefreshLayout
        swLayout.setColorSchemeResources(R.color.myColor4,R.color.myColor6);


        pref = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        id = pref.getString("id","");
        token = pref.getString("token","");

        mContext = getActivity().getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper
        my_recycler_view = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        try{
            loadJSON(token, id);
        }catch (Exception e){
            System.out.println(e);
        }

        my_recycler_view.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setAutoMeasureEnabled(false);
        my_recycler_view.setLayoutManager(llm);

        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // Handler untuk menjalankan jeda selama 5 detik
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {

                        // Berhenti berputar/refreshing
                        swLayout.setRefreshing(false);
                        loadJSON(token, id);

                        // fungsi-fungsi lain yang dijalankan saat refresh berhenti
//                        llayout.setBackground(mContext.getDrawable(RefreshViewActivity.this, R.drawable.ic_bg_navview));
//                        tvHello.setText("www.twoh.co");
                    }
                }, 5000);
            }
        });

        return view;
    }

    private void loadJSON(String token, String id_customer){
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMax(300);
        pDialog.setMessage("Harap Tunggu...");
        pDialog.setCancelable(false);
        pDialog.show();
        mApiService.getNotifikasi(token, id_customer).enqueue(new Callback<ArrayList<NotifikasiModel>>() {
            @Override
            public void onResponse(Call<ArrayList<NotifikasiModel>> call, Response<ArrayList<NotifikasiModel>> response) {
                pDialog.dismiss();
                ArrayList<NotifikasiModel> notifikasiItem = response.body();
                adapter = new NotifikasiAdapter(mContext, notifikasiItem);

                my_recycler_view.setAdapter(adapter);

                int count = notifikasiItem.size();
            }

            @Override
            public void onFailure(Call<ArrayList<NotifikasiModel>> call, Throwable t) {
                pDialog.dismiss();
            }
        });
    }
}
