package com.tonyhendra.tokosayur.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.tonyhendra.tokosayur.DetailProductActivity;
import com.tonyhendra.tokosayur.R;
import com.tonyhendra.tokosayur.model.SingleItemModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by tonyhendra on 07/12/2017.
 */

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {
    private ArrayList<SingleItemModel> itemsList;
    private Context mContext;
    NumberFormat formatRupiah;
    Locale localeID;

    public SectionListDataAdapter(Context context, ArrayList<SingleItemModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        final SingleItemModel singleItem = itemsList.get(i);
        localeID = new Locale("in", "ID");

        formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        holder.tvNama.setText(singleItem.getNama());
        Double harga = Double.parseDouble(singleItem.getHarga());
        holder.tvHarga.setText(formatRupiah.format((double)harga));

        //holder.id_product = singleItem.getId_product();
        Picasso.with(mContext)
                .load("http://192.168.43.25/ta/backend/web/uploads/"+singleItem.getImage())
                .placeholder(R.drawable.default_image)   // optional
                .error(R.drawable.default_image)      // optional
                .resize(300, 300)// optional
                .into(holder.itemImage);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DetailProductActivity.class);
                intent.putExtra("id_product", singleItem.getId_product());
                mContext.startActivity(intent);
            }
        });

       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvNama;
        protected TextView tvHarga;
        protected CardView cardView;

        protected ImageView itemImage;
        protected int id_product;

        public SingleItemRowHolder(View view) {
            super(view);

            this.tvNama = (TextView) view.findViewById(R.id.tvNama);
            this.tvHarga = (TextView) view.findViewById(R.id.tvHarga);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);
            this.cardView = (CardView)view.findViewById(R.id.cardView);


//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent intent = new Intent(mContext, DetailProductActivity.class);
//                    intent.putExtra("id_product", id_product);
//                    mContext.startActivity(intent);
//                    Toast.makeText(v.getContext(), tvIdProduct.getText(), Toast.LENGTH_SHORT).show();
//
//                }
//            });


        }

    }
}