package com.tonyhendra.tokosayur.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.squareup.picasso.Picasso;
import com.tonyhendra.tokosayur.R;

import java.util.List;

/**
 * Created by tonyh on 18-Mar-18.
 */

public class SliderAdapter extends PagerAdapter {

    private Context context;
    private List<String> img;
    private List<String> colorName;

    public SliderAdapter(Context context, List<String> img) {
        this.context = context;
        this.img = img;
        this.colorName = colorName;
    }

    @Override
    public int getCount() {
        return img.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_slider, null);

        //TextView textView = (TextView) view.findViewById(R.id.textView);
        ImageView imgView = (ImageView) view.findViewById(R.id.imgView);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout);

        //textView.setText(colorName.get(position));
        Picasso.with(context).load(img.get(position)).into(imgView);
        //linearLayout.setBackgroundColor(color.get(position));

        ViewPager viewPager = (ViewPager) container;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager) container;
        View view = (View) object;
        viewPager.removeView(view);
    }
}
