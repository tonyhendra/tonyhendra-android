package com.tonyhendra.tokosayur.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tonyhendra.tokosayur.R;
import com.tonyhendra.tokosayur.model.RuteDetailModel;

import java.util.ArrayList;

/**
 * Created by tonyh on 22-May-18.
 */

public class RuteDetailAdapter extends RecyclerView.Adapter<RuteDetailAdapter.RutedetailViewHolder> {
    private ArrayList<RuteDetailModel> ruteDetailModels;
    private int rowLayout;
    private Context mContext;

    public static class RutedetailViewHolder extends RecyclerView.ViewHolder{
        LinearLayout rutedetailLayout;
        TextView tvId;
        TextView tvAlamat;
        TextView tvNama;
        TextView tvNamaProduk;
        TextView tvJumlah;

        public RutedetailViewHolder(View v){
            super(v);
            rutedetailLayout = (LinearLayout)v.findViewById(R.id.rutedetailLayout);
            tvId = (TextView)v.findViewById(R.id.tvId);
            tvAlamat = (TextView)v.findViewById(R.id.tvAlamat);
            tvNama = (TextView)v.findViewById(R.id.tvNama);
            tvNamaProduk = (TextView)v.findViewById(R.id.tvNamaProduk);
            tvJumlah = (TextView)v.findViewById(R.id.tvJumlah);
        }

    }

    public RuteDetailAdapter(ArrayList<RuteDetailModel> ruteDetailModels, Context mContext){
        this.ruteDetailModels = ruteDetailModels;
        this.mContext = mContext;

    }

    @Override
    public RutedetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_rute_detail, null);
        RutedetailViewHolder mh = new RutedetailViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(RutedetailViewHolder holder, int position) {
        final RuteDetailModel ruteDetailModel = ruteDetailModels.get(position);
        holder.tvId.setText("Id : "+ruteDetailModel.getId());
        holder.tvAlamat.setText(ruteDetailModel.getAlamat());
        holder.tvNama.setText(ruteDetailModel.getNama());
        holder.tvNamaProduk.setText(ruteDetailModel.getProduct()+"("+ruteDetailModel.getJumlah()+")");
    }


    @Override
    public int getItemCount() {
        return (null != ruteDetailModels ? ruteDetailModels.size() : 0);
    }


}
