package com.tonyhendra.tokosayur.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tonyhendra.tokosayur.DetailOrderActivity;
import com.tonyhendra.tokosayur.R;
import com.tonyhendra.tokosayur.model.OrderItemModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by tonyh on 06-May-18.
 */

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderViewHolder> {
    private ArrayList<OrderItemModel> orderList;
    private int rowLayout;
    private Context mContext;
    NumberFormat formatRupiah;
    Locale localeID;

    public static class OrderViewHolder extends RecyclerView.ViewHolder{
        LinearLayout orderLayout;
        TextView tvNamaProduk;
        TextView tvHarga;
        TextView tvTanggal;
        TextView tvStatus;
        CardView cardView;
        //ImageView img_product;

        public OrderViewHolder(View v){
            super(v);
            orderLayout = (LinearLayout)v.findViewById(R.id.orderLayout);
            tvNamaProduk = (TextView)v.findViewById(R.id.tvNamaProduk);
            tvHarga = (TextView)v.findViewById(R.id.tvHarga);
            tvTanggal = (TextView)v.findViewById(R.id.tvTanggal);
            tvStatus = (TextView)v.findViewById(R.id.tvStatus);
            cardView = (CardView)v.findViewById(R.id.cardView);
            //img_product = (ImageView)v.findViewById(R.id.img_product);

        }

    }

    public OrderListAdapter(ArrayList<OrderItemModel> orderList, Context mContext){
        this.orderList = orderList;
        this.mContext = mContext;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_order, null);
        OrderViewHolder mh = new OrderViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(final OrderViewHolder holder, int position) {
        final OrderItemModel orderItemModel = orderList.get(position);
        localeID = new Locale("in", "ID");
        formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        holder.tvNamaProduk.setText("Order ID : "+orderItemModel.getId_order());
        Double harga = Double.parseDouble(orderItemModel.getTotal_harga());
        holder.tvHarga.setText(formatRupiah.format((double)harga));
        holder.tvTanggal.setText(orderItemModel.getTanggal());
        holder.tvStatus.setText(orderItemModel.getStatus());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, DetailOrderActivity.class);
                i.putExtra("id_order",orderItemModel.getId_order());
                i.putExtra("status",orderItemModel.getStatus());
                mContext.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != orderList ? orderList.size() : 0);
    }






}
