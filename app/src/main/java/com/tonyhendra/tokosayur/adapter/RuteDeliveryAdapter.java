package com.tonyhendra.tokosayur.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tonyhendra.tokosayur.R;
import com.tonyhendra.tokosayur.DriverRuteDetailActivity;
import com.tonyhendra.tokosayur.model.RuteModel;

import java.util.ArrayList;

/**
 * Created by tonyh on 06-May-18.
 */

public class RuteDeliveryAdapter extends RecyclerView.Adapter<RuteDeliveryAdapter.RutedeliveryViewHolder> {
    private ArrayList<RuteModel> ruteModels;
    private int rowLayout;
    private Context mContext;


    public static class RutedeliveryViewHolder extends RecyclerView.ViewHolder{
        LinearLayout rutepickupLayout;
        TextView tvIdRute;
        TextView tvJarak;
        TextView tvTanggal;
        TextView tvStatus;
        CardView cardView;
        //ImageView img_product;

        public RutedeliveryViewHolder(View v){
            super(v);
            rutepickupLayout = (LinearLayout)v.findViewById(R.id.rutepickupLayout);
            tvIdRute = (TextView)v.findViewById(R.id.tvIdRute);
            tvJarak = (TextView)v.findViewById(R.id.tvJarak);
            tvTanggal = (TextView)v.findViewById(R.id.tvTanggal);
            tvStatus = (TextView)v.findViewById(R.id.tvStatus);
            cardView = (CardView)v.findViewById(R.id.cardView);

            //img_product = (ImageView)v.findViewById(R.id.img_product);

        }

    }

    public RuteDeliveryAdapter(ArrayList<RuteModel> ruteModels, Context mContext){
        this.ruteModels = ruteModels;
        this.mContext = mContext;
    }

    @Override
    public RutedeliveryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_rute, null);
        RutedeliveryViewHolder mh = new RutedeliveryViewHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(RutedeliveryViewHolder holder, int position) {
        final RuteModel ruteModel = ruteModels.get(position);
        holder.tvIdRute.setText("ID Rute : "+ ruteModel.getId_rute());
        Double jarak = Double.parseDouble(ruteModel.getTotal_jarak());
        holder.tvJarak.setText("Total Jarak : "+String.format("%.2f", jarak)+" Km");
        holder.tvTanggal.setText(ruteModel.getTanggal());
        holder.tvStatus.setText(ruteModel.getStatus());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, DriverRuteDetailActivity.class);
                i.putExtra("id_rute", ruteModel.getId_rute());
                i.putExtra("path", "rute-delivery");
                mContext.startActivity(i);
            }
        });
        //holder.id_product = singleItem.getId_product();
//        Picasso.with(mContext)
//                .load(R.drawable.default_image)
//                .placeholder(R.drawable.default_image)   // optional
//                .error(R.drawable.default_image)      // optional
//                .resize(300, 300)// optional
//                .into(holder.img_product);
    }

    @Override
    public int getItemCount() {
        return (null != ruteModels ? ruteModels.size() : 0);
    }






}
