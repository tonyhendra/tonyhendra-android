package com.tonyhendra.tokosayur.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tonyhendra.tokosayur.DetailProductActivity;
import com.tonyhendra.tokosayur.R;
import com.tonyhendra.tokosayur.model.CartItemModel;
import com.tonyhendra.tokosayur.model.NotifikasiModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by tonyhendra on 07/12/2017.
 */

public class NotifikasiAdapter extends RecyclerView.Adapter<NotifikasiAdapter.NotifikasiRowHolder> {
    private ArrayList<NotifikasiModel> itemsList;
    private Context mContext;

    public NotifikasiAdapter(Context context, ArrayList<NotifikasiModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public NotifikasiRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item_notifikasi, null);
        NotifikasiRowHolder mh = new NotifikasiRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(NotifikasiRowHolder holder, int i) {

        final NotifikasiModel notifikasiItem = itemsList.get(i);

        holder.tvTanggal.setText(notifikasiItem.getTanggal());
        holder.tvPesan.setText(notifikasiItem.getPesan());

    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class NotifikasiRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTanggal;
        protected TextView tvPesan;

        public NotifikasiRowHolder(View view) {
            super(view);

            this.tvTanggal = (TextView) view.findViewById(R.id.tvTanggal);
            this.tvPesan = (TextView) view.findViewById(R.id.tvPesan);

//            view.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    Intent intent = new Intent(mContext, DetailProductActivity.class);
//                    intent.putExtra("id_product", id_product);
//                    mContext.startActivity(intent);
//                    Toast.makeText(v.getContext(), tvIdProduct.getText(), Toast.LENGTH_SHORT).show();
//
//                }
//            });


        }

    }
}