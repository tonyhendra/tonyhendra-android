package com.tonyhendra.tokosayur.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.tonyhendra.tokosayur.FragmentNotifikasi;
import com.tonyhendra.tokosayur.FragmentHome;
import com.tonyhendra.tokosayur.FragmentProfile;
import com.tonyhendra.tokosayur.FragmentOrders;

/**
 * Created by tonyhendra on 26/11/2017.
 */

public class MainAdapter extends FragmentStatePagerAdapter {

    public MainAdapter(FragmentManager fragmentManager){
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FragmentHome();
            case 1:
                return new FragmentOrders();
            case 2:
                return new FragmentNotifikasi();
            case 3:
                return new FragmentProfile();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
