package com.tonyhendra.tokosayur;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KonfirmasiPembayaranActivity extends AppCompatActivity {
    Context mContext;
    BaseApiService mApiService;
    SharedPreferences pref;
    Toolbar toolbar;
    ProgressDialog pDialog;
    Button btnSubmit;
    TextView tvIdOrder;
    EditText etNama, etNorek, etBank, etNominal;
    Spinner spTujuan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_konfirmasi_pembayaran);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvIdOrder = (TextView)findViewById(R.id.tvIdOrder);
        etNama = (EditText)findViewById(R.id.etNama);
        etNorek = (EditText)findViewById(R.id.etNorek);
        etBank = (EditText)findViewById(R.id.etBank);
        etNominal = (EditText)findViewById(R.id.etNominal);
        spTujuan = (Spinner)findViewById(R.id.spTujuan);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);

        pref = this.getSharedPreferences("data", Context.MODE_PRIVATE);
        String token = pref.getString("token","");
        final String id_order;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                id_order= null;
            } else {
                id_order= extras.getString("id_order");
            }
        } else {
            id_order = (String) savedInstanceState.getSerializable("STRING_I_NEED");
        }
        tvIdOrder.setText("Order Number : "+id_order);
        mContext = this.getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etNama.getText().toString().trim().length()==0){
                    etNama.setError("Nama tidak boleh kosong");
                }
                else if(etNorek.getText().toString().trim().length()==0){
                    etNorek.setError("No Rekening tidak boleh kosong");
                }
                else if(etBank.getText().toString().trim().length()==0){
                    etBank.setError("Nama Bank tidak boleh kosong");
                }
                else if(etNominal.getText().toString().trim().length()==0){
                    etNominal.setError("Nominal transfer tidak boleh kosong");
                }
                else{
                    konfirmasiPembayaran(Integer.parseInt(id_order),etNama.getText().toString(),etBank.getText().toString(),
                            etNorek.getText().toString(),Double.parseDouble(etNominal.getText().toString()),spTujuan.getSelectedItem().toString());
                }
            }
        });


    }

    public void konfirmasiPembayaran(int id_order, String nama, String bank, String no_rek, Double nominal, String tujuan ){
        pDialog = new ProgressDialog(this);
        pDialog.setMax(30);
        pDialog.setMessage("Harap Tunggu...");
        pDialog.show();
        mApiService.konfirmasiPembayaran(id_order,nama,bank,no_rek,nominal,tujuan).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if(jsonObject.getString("status").equals("success")){
//                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    switch (which){
//                                        case DialogInterface.BUTTON_POSITIVE:
//                                            //Yes button clicked
                                            Intent i = new Intent(KonfirmasiPembayaranActivity.this, MainActivity.class);
                                            startActivity(i);
//                                            break;
//                                    }
//                                }
//                            };

                           //AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.myDialog));
                            //builder.setMessage("Pembayaran anda sedang dalam proses verifikasi").setPositiveButton("Ok", dialogClickListener).show();
                        }else {
                            Snackbar.make(findViewById(android.R.id.content),"Error",Snackbar.LENGTH_LONG);
                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }else {
                    pDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}
