package com.tonyhendra.tokosayur;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.tonyhendra.tokosayur.R;
import com.tonyhendra.tokosayur.adapter.OrderListAdapter;
import com.tonyhendra.tokosayur.model.OrderItemModel;
import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tonyh on 08-Apr-18.
 */

public class FragmentOrders extends Fragment {
    SharedPreferences pref;
    String id, token;
    Context mContext;
    BaseApiService mApiService;
    RecyclerView recyclerView;
    ArrayList<OrderItemModel> orderItemModel;
    OrderListAdapter adapter;
    ProgressDialog pDialog;
    SwipeRefreshLayout swLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orders,container,false);

        pref = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        id = pref.getString("id","");
        token = pref.getString("token","");

        swLayout = (SwipeRefreshLayout) view.findViewById(R.id.swlayout);
        // Mengeset properti warna yang berputar pada SwipeRefreshLayout
        swLayout.setColorSchemeResources(R.color.myColor4,R.color.myColor6);


        mContext = getActivity().getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper

        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this.getActivity());
        llm.setAutoMeasureEnabled(false);
        recyclerView.setLayoutManager(llm);
        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {

                        // Berhenti berputar/refreshing
                        swLayout.setRefreshing(false);
                        getOrderList(token,id);

                        // fungsi-fungsi lain yang dijalankan saat refresh berhenti
//                        llayout.setBackground(mContext.getDrawable(RefreshViewActivity.this, R.drawable.ic_bg_navview));
//                        tvHello.setText("www.twoh.co");
                    }
                }, 5000);
            }
        });
        getOrderList(token,id);

        return view;

    }

    public void getOrderList(String token, String id_customer){
        //pDialog = new ProgressDialog(getActivity());
//        pDialog.setMax(30);
//        pDialog.show();
        mApiService.getOrders(token, id_customer).enqueue(new Callback<ArrayList<OrderItemModel>>() {
            @Override
            public void onResponse(Call<ArrayList<OrderItemModel>> call, Response<ArrayList<OrderItemModel>> response) {

                    //pDialog.dismiss();
                    ArrayList<OrderItemModel> orderItemModels = response.body();

                    adapter = new OrderListAdapter(orderItemModels, mContext);
                    recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<ArrayList<OrderItemModel>> call, Throwable t) {
                //pDialog.dismiss();
                Snackbar.make(getActivity().findViewById(android.R.id.content),"Error",Snackbar.LENGTH_SHORT).show();

            }
        });
    }
}
