package com.tonyhendra.tokosayur;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.tonyhendra.tokosayur.adapter.RuteDetailAdapter;
import com.tonyhendra.tokosayur.model.RuteDetailModel;
import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverRuteDetailActivity extends AppCompatActivity {
    SharedPreferences pref;
    String id, token, id_rute, path;
    Context mContext;
    Toolbar toolbar;
    BaseApiService mApiService;
    RecyclerView recyclerView;
    ArrayList<RuteDetailModel> ruteDetailModel;
    RuteDetailAdapter adapter;
    ProgressDialog pDialog;
    Button btnSelesai;
    String depot;
    String mapsUri;
    String latlngUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_rute_detail);

        pref =getSharedPreferences("data", Context.MODE_PRIVATE);
        id = pref.getString("id","");
        token = pref.getString("token","");


        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                id_rute= null;
                path = null;
            } else {
                id_rute= extras.getString("id_rute");
                path= extras.getString("path");
            }
        } else {
            id_rute = savedInstanceState.getSerializable("STRING_I_NEED").toString();
            path = savedInstanceState.getSerializable("STRING_I_NEED").toString();
        }

        mContext = getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper


        btnSelesai = (Button)findViewById(R.id.btnSelesai);
        recyclerView = (RecyclerView)findViewById(R.id.recycler_view_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setAutoMeasureEnabled(false);
        recyclerView.setLayoutManager(llm);
        getRuteDetail(path,id,token,id_rute);
        getLatLngDepot();
        btnSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //getDirection(path,token,id_rute);
                konfirmasi(path,token,id_rute);
            }
        });

    }

    public void getRuteDetail(String path,String id, String token, String id_rute){
        pDialog = new ProgressDialog(this);
        pDialog.setMax(30);
        pDialog.setMessage("Harap Tunggu ...");
        pDialog.setCancelable(false);
        pDialog.show();
        mApiService.getRuteDetail(path, token, id, id_rute).enqueue(new Callback<ArrayList<RuteDetailModel>>() {
            @Override
            public void onResponse(Call<ArrayList<RuteDetailModel>> call, Response<ArrayList<RuteDetailModel>> response) {
                pDialog.dismiss();
                ArrayList<RuteDetailModel> ruteDetailModels = response.body();

                adapter = new RuteDetailAdapter(ruteDetailModels, mContext);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<ArrayList<RuteDetailModel>> call, Throwable t) {
                pDialog.dismiss();
            }
        });
    }

     public void getLatLngDepot(){
        mApiService.getDepot().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try{
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    depot = jsonObject.getString("lat")+","+jsonObject.getString("lng");
                }catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void getDirection(String path, String token, String id_rute){
        pDialog = new ProgressDialog(this);
        pDialog.setMax(30);
        pDialog.setMessage("Harap Tunggu ...");
        pDialog.setCancelable(false);
        pDialog.show();
        mApiService.getRuteDirection(path,token, id_rute).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pDialog.dismiss();
                try{
                    //JSONObject jsonRESULTS = new JSONObject(response.body().string());
                    JSONArray jsonArray = new JSONArray(response.body().string());
                    getLatLngDepot();
                    String latlng = "";
                    for (int i=0; i<jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        if(i==0){
                            latlng = jsonObject.getString("lat")+","+jsonObject.getString("lng");
                        }else{
                            latlng = latlng+"|"+jsonObject.getString("lat")+","+jsonObject.getString("lng");
                        }
                    }
                    mapsUri = "https://www.google.com/maps/dir/?api=1&origin="+depot+"&destination="+depot+"&waypoints="+latlng+"&travelmode=driving";
                    Log.d("mapsUri",mapsUri);

                    String uri = mapsUri;
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(Intent.createChooser(intent, "com.google.android.apps.maps"));

                }catch (JSONException e) {
                    e.printStackTrace();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void konfirmasi(String path, String token, String id_rute){
        pDialog = new ProgressDialog(this);
        pDialog.setMax(30);
        pDialog.show();
        mApiService.konfirmasi(path, token, id_rute).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.maps, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.map) {
            getDirection(path,token,id_rute);
        }
        return super.onOptionsItemSelected(item);
    }



}
