package com.tonyhendra.tokosayur;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by tonyhendra on 26/11/2017.
 */

public class FragmentProfile extends Fragment {
    TextView tvResultNama, tvJenisKelamin;
    EditText etUsername, etPassword, etNama, etEmail, etTanggaLahir, etNoHp;
    String resultNama;
    Button btnLogout, btnSimpan, btnUbah;
    SharedPreferences pref;
    BaseApiService mApiService;
    Context mContext;
    ProgressDialog pDialog;
    RadioGroup rgJenisKelamin;
    RadioButton  rbLakiLaki, rbPerempuan;
    Calendar calendar;
    DatePickerDialog datePickerDialog;
    ImageView userPhoto;
    String jeniskelamin, password;
    SwipeRefreshLayout swLayout;
    String username,token;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        pref = getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        mContext = getActivity().getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper

        username = pref.getString("username", "");
        token = pref.getString("token", "");

        swLayout = (SwipeRefreshLayout) view.findViewById(R.id.swlayout);
        // Mengeset properti warna yang berputar pada SwipeRefreshLayout
        swLayout.setColorSchemeResources(R.color.myColor4,R.color.myColor6);

        tvResultNama = (TextView) view.findViewById(R.id.tvResultUsername);
        tvJenisKelamin = (TextView) view.findViewById(R.id.tvJenisKelamin);
        btnLogout = (Button) view.findViewById(R.id.btnLogout);
        etUsername = (EditText) view.findViewById(R.id.etUsername);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        etNama = (EditText) view.findViewById(R.id.etNama);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        etTanggaLahir = (EditText)view.findViewById(R.id.etTanggalLahir);
        etNoHp = (EditText)view.findViewById(R.id.etNoHp);
        rgJenisKelamin = (RadioGroup)view.findViewById(R.id.rgJenisKelamin);
        rbLakiLaki = (RadioButton)view.findViewById(R.id.rbLakiLaki);
        rbPerempuan = (RadioButton)view.findViewById(R.id.rbPerempuan);
        btnSimpan = (Button)view.findViewById(R.id.btnSimpan);
        btnUbah = (Button)view.findViewById(R.id.btnUbah);
        calendar = Calendar.getInstance();
        userPhoto = (ImageView)view.findViewById(R.id.user_photo);

        datePickerDialog = new DatePickerDialog(
                getActivity(),datePickerListener, calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));

        etTanggaLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });


        getProfile(token,username);
        componentDisabled();
        // untuk mendapatkan data dari activity sebelumnya, yaitu activity login.
//        Bundle extras = getIntent().getExtras();
//        if (extras != null)
//            resultNama = extras.getString("result_username");

        //tvResultNama.setText(pref.getString("username",""));
        password = etPassword.getText().toString();
        etPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPassword.setText("");
            }
        });
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("token","");
                editor.putString("username","");
                editor.putInt("login",0);
                editor.clear();
                editor.commit();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        //((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        btnUbah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                componentEnabled();
            }
        });
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedId = rgJenisKelamin.getCheckedRadioButtonId();
                switch (selectedId){
                    case R.id.rbLakiLaki :
                        jeniskelamin = rbLakiLaki.getText().toString();
                        break;
                    case R.id.rbPerempuan :
                        jeniskelamin = rbPerempuan.getText().toString();
                        break;
                }
                if(etPassword.getText().toString()==null){
                    etPassword.setText(password);
                }
                componentDisabled();
                updateProfile(etUsername.getText().toString(),etEmail.getText().toString(),etPassword.getText().toString(),
                        etNama.getText().toString(),etTanggaLahir.getText().toString(),
                        jeniskelamin,etNoHp.getText().toString());
            }
        });

        swLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // Handler untuk menjalankan jeda selama 5 detik
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {

                        // Berhenti berputar/refreshing
                        swLayout.setRefreshing(false);
                        getProfile(token,username);

                    }
                }, 5000);
            }
        });
        return view;
    }


    private void getProfile(String token, String username){
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMax(30);
        pDialog.setMessage("Harap Tunggu ...");
        pDialog.setCancelable(false);
        pDialog.show();
        mApiService.getProfile(token, username).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pDialog.dismiss();
                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("status").equals("success")){
                            String nama = jsonRESULTS.getJSONObject("data").getString("nama");
                            String username_ = jsonRESULTS.getJSONObject("data").getString("username");
                            String email = jsonRESULTS.getJSONObject("data").getString("email");
                            String tanggal_lahir = jsonRESULTS.getJSONObject("data").getString("tanggal_lahir");
                            String jenis_kelamin = jsonRESULTS.getJSONObject("data").getString("jenis_kelamin");
                            String no_hp = jsonRESULTS.getJSONObject("data").getString("no_hp");
                            String password = jsonRESULTS.getJSONObject("data").getString("password_hash");

                            tvResultNama.setText(nama);
                            etNama.setText(nama);
                            etUsername.setText(username_);
                            etEmail.setText(email);
                            etPassword.setText(password);
                            etTanggaLahir.setText(tanggal_lahir);
                            etNoHp.setText(no_hp);
                            if(jenis_kelamin.equals("Laki-Laki")){
                                rbLakiLaki.setChecked(true);
                                userPhoto.setImageResource(R.drawable.man);
                            }else{
                                rbPerempuan.setChecked(true);
                                userPhoto.setImageResource(R.drawable.women);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pDialog.dismiss();

            }
        });
    }

    private void updateProfile(String username, String email, String password, String nama, String tanggal_lahir, String jenis_kelamin, String no_hp){
        pDialog = new ProgressDialog(getActivity());
        pDialog.setMax(30);
        pDialog.show();
        mApiService.updateProfile(username, email, password, nama, tanggal_lahir, jenis_kelamin, no_hp).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pDialog.dismiss();
                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("status").equals("success")) {
                            // Jika register berhasil
                            String message = jsonRESULTS.getString("message");
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        }else {
                            // Jika register gagal
                            String message = "Gagal menyimpan";
                            Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pDialog.dismiss();

            }
        });
    }

    private void componentEnabled(){
        tvJenisKelamin.setEnabled(true);
        etUsername.setEnabled(true);
        etNama.setEnabled(true);
        etEmail.setEnabled(true);
        etPassword.setEnabled(true);
        etNoHp.setEnabled(true);
        etTanggaLahir.setEnabled(true);
        rbPerempuan.setEnabled(true);
        rbLakiLaki.setEnabled(true);
        btnSimpan.setVisibility(View.VISIBLE);
        btnUbah.setVisibility(View.INVISIBLE);
        btnSimpan.setEnabled(true);

    }

    private void componentDisabled(){
        tvJenisKelamin.setEnabled(false);
        etUsername.setEnabled(false);
        etNama.setEnabled(false);
        etEmail.setEnabled(false);
        etPassword.setEnabled(false);
        etNoHp.setEnabled(false);
        etTanggaLahir.setEnabled(false);
        rbPerempuan.setEnabled(false);
        rbLakiLaki.setEnabled(false);
        btnUbah.setEnabled(true);
        btnUbah.setVisibility(View.VISIBLE);
        btnSimpan.setVisibility(View.INVISIBLE);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "yyyy-MM-dd"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            etTanggaLahir.setText(sdf.format(calendar.getTime()));
        }
    };

}
