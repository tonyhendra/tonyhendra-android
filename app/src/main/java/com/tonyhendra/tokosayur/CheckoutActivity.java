package com.tonyhendra.tokosayur;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Build;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.SyncStateContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity  extends AppCompatActivity  {
    Context mContext;
    BaseApiService mApiService;
    ProgressDialog pDialog;

    private Button btPlacesAPI, btnMin, btnPls, btnBuy;
    private TextView tvLat,tvLng,tvHarga, tvNamaProduk;
    private EditText etAlamat, etJumlah;
    private ImageView img_product;
    // konstanta untuk mendeteksi hasil balikan dari place picker
    private int PLACE_PICKER_REQUEST = 1;
    Integer jml, id_customer, id_product;
    SharedPreferences pref;
    NumberFormat formatRupiah;
    Toolbar toolbar;

    String nama, harga, berat, urlImg, token;

    Double total_harga, total_berat, lat, lng;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Checkout");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mContext = getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper
        pref = this.getSharedPreferences("data", Context.MODE_PRIVATE);
        token = pref.getString("token","");
        id_customer = Integer.parseInt(pref.getString("id", ""));
        final String username = pref.getString("username","");

        Locale localeID;
        localeID = new Locale("in", "ID");

        formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                id_product= null;
            } else {
                id_product= Integer.parseInt(extras.getString("id_product"));
                harga = extras.getString("harga");
                berat = extras.getString("berat");
                urlImg = extras.getString("urlImg");
                nama = extras.getString("nama");
            }
        } else {
            id_product = Integer.parseInt(savedInstanceState.getSerializable("STRING_I_NEED").toString());
        }

        tvLat = (TextView) findViewById(R.id.tvLat);
        tvLng = (TextView) findViewById(R.id.tvLng);
        tvHarga = (TextView)findViewById(R.id.tvHarga);
        tvNamaProduk = (TextView) findViewById(R.id.tvNamaProduk);
        etAlamat = (EditText) findViewById(R.id.etAlamat);
        etJumlah = (EditText)findViewById(R.id.etJumlah);
        btnMin = (Button)findViewById(R.id.btnMin);
        btnPls = (Button)findViewById(R.id.btnPls);
        btnBuy = (Button)findViewById(R.id.btnBuy);
        etJumlah.setText("1");
        img_product = (ImageView)findViewById(R.id.img_product);
        jml = Integer.parseInt(etJumlah.getText().toString());

        total_harga = jml*Double.parseDouble(harga);
        total_berat = jml*Double.parseDouble(berat);
        tvHarga.setText(formatRupiah.format(total_harga));
        tvNamaProduk.setText(nama);
        Picasso.with(mContext)
                .load(urlImg)
                .placeholder(R.drawable.default_image)   // optional
                .error(R.drawable.default_image)      // optional
                .resize(300, 300)// optional
                .into(img_product);

        btPlacesAPI = (Button)findViewById(R.id.btnLocation);
        btPlacesAPI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // membuat Intent untuk Place Picker
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    //menjalankan place picker
                    startActivityForResult(builder.build(CheckoutActivity.this), PLACE_PICKER_REQUEST);
                    // check apabila <a title="Solusi Tidak Bisa Download Google Play Services di Android" href="http://www.twoh.co/2014/11/solusi-tidak-bisa-download-google-play-services-di-android/" target="_blank">Google Play Services tidak terinstall</a> di HP
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        etJumlah.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {
                try{
                    if(editable.toString().equals("")||editable==null){
                        total_harga = 0*Double.parseDouble(harga);
                        total_berat = 0*Double.parseDouble(berat);
                        tvHarga.setText(formatRupiah.format(total_harga));
                    }else {
                        total_harga = Double.parseDouble(editable.toString())*Double.parseDouble(harga);
                        total_berat = Double.parseDouble(editable.toString())*Double.parseDouble(berat);
                        tvHarga.setText(formatRupiah.format(total_harga));
                    }
                }catch (Exception e){

                }


            }
        });
        btnMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jml = jml-1;
                if(jml<1){
                    etJumlah.setText(jml.toString());
                    onBackPressed();
                }else{
                    Double total = jml*Double.parseDouble(harga);
                    tvHarga.setText(formatRupiah.format(total));
                    etJumlah.setText(jml.toString());
                }

            }
        });

        btnPls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jml = jml+1;
                Double total = jml*Double.parseDouble(harga);
                tvHarga.setText(formatRupiah.format(total));
                etJumlah.setText(jml.toString());
            }
        });

        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etAlamat.getText().toString().trim().length()!=0){
                    createOrder(token, id_customer, etAlamat.getText().toString(), lat, lng, total_berat, total_harga, id_product, jml);
                }else{
                    etAlamat.setError("masukkan alamat pengiriman");
                    Snackbar.make(view, "Masukkan alamat pengiriman", Snackbar.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // menangkap hasil balikan dari Place Picker, dan menampilkannya pada TextView
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                tvLat.setText("Lat : "+String.valueOf(place.getLatLng().latitude));
                lat = place.getLatLng().latitude;
                tvLng.setText("Lng : "+String.valueOf(place.getLatLng().longitude));
                lng = place.getLatLng().longitude;
                String alamat = String.format(
                        "%s \n" +
                                "%s \n", place.getName(), place.getAddress());
                etAlamat.setText(alamat);
//                String toastMsg = String.format(
//                        "Place: %s \n" +
//                                "Alamat: %s \n" +
//                                "Latlng %s \n", place.getName(), place.getAddress(), place.getLatLng().latitude+" "+place.getLatLng().longitude);
                //tvPlaceAPI.setText(toastMsg);
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(CheckoutActivity.this);

        //Setting message manually and performing action on button click
        builder.setMessage("Anda ingin membatalkan pesanan?")
                .setCancelable(false)
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        CheckoutActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        if(jml==0){
                            jml = 1;
                            etJumlah.setText("1");
                        }
                        dialog.cancel();
                    }
                });

        //Creating dialog box
        android.support.v7.app.AlertDialog alert = builder.create();
        //Setting the title manually
        //alert.setTitle("AlertDialogExample");
        alert.show();
    }

    public void createOrder(String token, Integer id_customer, String alamat, Double lat, Double lng, Double total_berat, final Double total_harga, Integer id_product, final Integer jumlah){
        pDialog = new ProgressDialog(CheckoutActivity.this);
        pDialog.setMax(30);
        pDialog.setMessage("Harap Tunggu...");
        pDialog.show();
        mApiService.createOrder(token, id_customer, alamat, lat, lng, total_berat, total_harga, id_product, jumlah).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                pDialog.dismiss();
                Toast.makeText(mContext, "Berhasil", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(CheckoutActivity.this, InfoPembayaranActivity.class);
                i.putExtra("nama",tvNamaProduk.getText().toString());
                i.putExtra("jumlah", jml);
                i.putExtra("harga", harga);
                i.putExtra("total_harga", total_harga);
                startActivity(i);

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pDialog.dismiss();

            }
        });
    }


}