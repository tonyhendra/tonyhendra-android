package com.tonyhendra.tokosayur;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    EditText etUsername;
    EditText etPassword;
    Button btnLogin;
    Button btnRegister;
    ProgressDialog loading;

    Context mContext;
    BaseApiService mApiService;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        pref = PreferenceManager.getDefaultSharedPreferences(this);
        pref = getSharedPreferences("data", MODE_PRIVATE);
        pref = getSharedPreferences("data", Context.MODE_PRIVATE);
        final int login = pref.getInt("login", 0);
        final String role = pref.getString("role","");

        if(login == 1){
            switch (role){
                case "1":
                    break;
                case "2":
                    Intent i2 = new Intent(LoginActivity.this, DriverMainActivity.class);
                    startActivity(i2);
                    this.finish();
                    break;
                case "3":
                    Intent i3 = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i3);
                    this.finish();
                    break;
            }

        }
//        else if(login == 1 && role=="2") {
//            Intent intent = new Intent(LoginActivity.this, DriverMainActivity.class);
//            startActivity(intent);
//            this.finish();
//        }


        mContext = this;
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper
        initComponents();
    }



    public void initComponents(){
        etUsername = (EditText) findViewById(R.id.etUsername);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegister = (Button) findViewById(R.id.btnRegister);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etUsername.getText().toString().trim().length()==0){
                    etUsername.setError("Username tidak boleh kosong");
                    Snackbar.make(v, "Username tidak boleh kosong", Snackbar.LENGTH_SHORT).show();
                }else if(etPassword.getText().toString().trim().length()==0){
                    etPassword.setError("Password tidak boleh kosong");
                    Snackbar.make(v, "Password tidak boleh kosong", Snackbar.LENGTH_SHORT).show();
                }else {
                    loading = ProgressDialog.show(mContext, null, "Harap Tunggu...", true, false);
                    requestLogin();
                }
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, RegisterActivity.class));
            }
        });
    }
    private void requestLogin(){
        mApiService.loginRequest(etUsername.getText().toString(), etPassword.getText().toString())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()){
                            loading.dismiss();
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("status").equals("success")){
                                    // Jika login berhasil maka data nama yang ada di response API
                                    // akan diparsing ke activity selanjutnya.
                                    String username = jsonRESULTS.getJSONObject("data").getString("username");
                                    String token = jsonRESULTS.getJSONObject("data").getString("token");
                                    String nama = jsonRESULTS.getJSONObject("data").getString("nama");
                                    String id = jsonRESULTS.getJSONObject("data").getString("id");
                                    String role = jsonRESULTS.getJSONObject("data").getString("role");
                                    SharedPreferences.Editor editor = pref.edit();
                                    editor.putString("username", username);
                                    editor.putString("nama", nama);
                                    editor.putString("token",token);
                                    editor.putString("id", id);
                                    editor.putInt("login", 1);
                                    editor.putString("role",role);
                                    editor.commit();



                                    Toast.makeText(mContext, "Berhasil Login", Toast.LENGTH_SHORT).show();

                                    Intent intent;

                                    if(role.equals("3")){
                                        intent = new Intent(mContext, MainActivity.class);
                                        intent.putExtra("result_username", username);
                                    }else {
                                        intent = new Intent(mContext, DriverMainActivity.class);
                                        intent.putExtra("result_username", username);
                                    }
                                    startActivity(intent);
                                    finish();
                                } else {
                                    // Jika login gagal
                                    String error_message = jsonRESULTS.getString("message");
                                    Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                                    Snackbar.make(findViewById(android.R.id.content), error_message, Snackbar.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            loading.dismiss();
                            Snackbar.make(findViewById(android.R.id.content), "Tidak bisa terhubung ke server", Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("debug", "onFailure: ERROR > " + t.toString());
                        loading.dismiss();
                        Snackbar.make(findViewById(android.R.id.content), "Tidak bisa terhubung ke server", Snackbar.LENGTH_LONG).show();
                    }
                });
    }


}
