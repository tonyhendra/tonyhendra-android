package com.tonyhendra.tokosayur;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverMainActivity extends AppCompatActivity {

    private Button btnPickup, btnProfile , btnDelivery;
    SharedPreferences pref;
    Toolbar toolbar;
    String id, token, deliveryCount, pickupCount;
    Context mContext;
    BaseApiService mApiService;
    ProgressDialog pDialog;

    TextView tvNama, tvNotifikasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_main);

        FirebaseMessaging.getInstance().subscribeToTopic("driver");

        pref = getSharedPreferences("data", Context.MODE_PRIVATE);
        String nama = pref.getString("nama","");
        id = pref.getString("id","");
        token = pref.getString("token","");

        mContext = getApplicationContext();
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper
        tvNama = (TextView)findViewById(R.id.tvNama);
        tvNama.setText("Hello, "+nama);
        tvNotifikasi = (TextView)findViewById(R.id.tvNotifikasi);

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnProfile = (Button)findViewById(R.id.btnProfile);
        btnPickup = (Button)findViewById(R.id.btnPickup);
        btnDelivery = (Button)findViewById(R.id.btnDelivery);
        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverMainActivity.this, DriverProfileActivity.class);
                startActivity(intent);
            }
        });
        btnDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverMainActivity.this, DriverRuteDeliveryActivity.class);
                startActivity(intent);
            }
        });
        btnPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DriverMainActivity.this, DriverRutePickupActivity.class);
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getJobCount(token, id);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the main_menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.logout:
                //your action
                logOut();
                break;
            case R.id.exit:
                //your action
                break;
            default:
                return super.onOptionsItemSelected(item);
        }

        return true;
    }

    private void getJobCount(String token, String id_driver){
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Harap Tunggu ...");
        pDialog.setCancelable(false);
        pDialog.setMax(30);
        pDialog.show();
        mApiService.getJobCount(token, id_driver).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    pDialog.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response.body().string());
                        JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                        String count_pickup = jsonObject1.getString("count_pickup");
                        JSONObject jsonObject2 = jsonArray.getJSONObject(1);
                        String count_delivery = jsonObject2.getString("count_delivery");
                        if(!count_pickup.equals("0") && !count_delivery.equals("0")){
                            tvNotifikasi.setText("Anda memiliki "+count_pickup+" pickup jobs dan "+count_delivery+" delivery jobs yang belum terselesaikan!");
                        }else if(!count_pickup.equals("0") && count_delivery.equals("0")){
                            tvNotifikasi.setText("Anda memiliki "+count_pickup+" pickup jobs yang belum terselesaikan!");
                        }else{
                            tvNotifikasi.setText("Anda memiliki "+count_delivery+" delivery jobs yang belum terselesaikan!");
                        }

                    }catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    pDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pDialog.dismiss();
            }
        });
    }
    private void logOut(){
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("token","");
        editor.putString("username","");
        editor.putInt("login",0);
        editor.clear();
        editor.commit();

        Intent intent = new Intent(DriverMainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

}
