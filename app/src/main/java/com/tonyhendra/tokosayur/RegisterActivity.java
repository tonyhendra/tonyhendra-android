package com.tonyhendra.tokosayur;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.tonyhendra.tokosayur.utils.BaseApiService;
import com.tonyhendra.tokosayur.utils.UtilsApi;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    EditText etUsername;
    EditText etEmail;
    EditText etPassword;
    EditText etTanggalLahir;
    EditText etNama;
    EditText etNoHp;
    RadioGroup rgJenisKelamin;
    RadioButton rbLakiLaki, rbPerempuan;
    Button btnRegister;
    ProgressDialog loading;
    String status, jeniskelamin;
    Calendar calendar;

    BaseApiService mApiService;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mContext = this;
        mApiService = UtilsApi.getAPIService(); // meng-init yang ada di package apihelper

        calendar = Calendar.getInstance();
        initComponents();
        etUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                cekUsername(etUsername.getText().toString());
            }
        });




    }

    public void initComponents(){
        etUsername = (EditText)findViewById(R.id.etUsername);
        etEmail  = (EditText)findViewById(R.id.etEmail);
        etPassword = (EditText)findViewById(R.id.etPassword);
        btnRegister = (Button)findViewById(R.id.btnRegister);
        etNama = (EditText)findViewById(R.id.etNama);
        etNoHp = (EditText)findViewById(R.id.etNoHp);
        rgJenisKelamin = (RadioGroup)findViewById(R.id.rgJenisKelamin);
        rbLakiLaki = (RadioButton)findViewById(R.id.rbLakiLaki);
        rbPerempuan = (RadioButton)findViewById(R.id.rbPerempuan);
        etTanggalLahir  = (EditText)findViewById(R.id.etTanggalLahir);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(
                mContext, RegisterActivity.this, calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etUsername.getText().toString().trim().length()==0){
                    etUsername.setError("Username tidak boleh kosong");
                    Snackbar.make(view, "Username tidak boleh kosong", Snackbar.LENGTH_SHORT).show();
                }
                else if(etEmail.getText().toString().trim().length()==0){
                    etEmail.setError("Email tidak boleh kosong");
                    Snackbar.make(view, "Email tidak boleh kosong", Snackbar.LENGTH_SHORT).show();
                }
                else if(etNama.getText().toString().trim().length()==0){
                    etNama.setError("Nama tidak boleh kosong");
                    Snackbar.make(view, "Nama tidak boleh kosong", Snackbar.LENGTH_SHORT).show();
                }
                else if(etPassword.getText().toString().trim().length()==0){
                    etPassword.setError("Password tidak boleh kosong");
                    Snackbar.make(view, "Password tidak boleh kosong", Snackbar.LENGTH_SHORT).show();
                }
                else if(etTanggalLahir.getText().toString().trim().length()==0){
                    etTanggalLahir.setError("Tanggal lahir tidak boleh kosong");
                    Snackbar.make(view, "Tanggal lahir tidak boleh kosong", Snackbar.LENGTH_SHORT).show();
                }else {
                    int selectedId = rgJenisKelamin.getCheckedRadioButtonId();
                    switch (selectedId){
                        case R.id.rbLakiLaki :
                            jeniskelamin = rbLakiLaki.getText().toString();
                            break;
                        case R.id.rbPerempuan :
                            jeniskelamin = rbPerempuan.getText().toString();
                            break;
                    }
                    loading = ProgressDialog.show(mContext,null,"Harap Tunggu ...",true, false);
                    requestRegister();
                }
            }
        });
        etTanggalLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show();
            }
        });
    }
    private void cekUsername(String username){
        mApiService.cekUsername(username).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){

                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("status").equals("error")){
                            etUsername.setError("Username tidak tersedia");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void requestRegister(){
        mApiService.registerRequest(etUsername.getText().toString(),etEmail.getText().toString(),
                etPassword.getText().toString(),etNama.getText().toString(),etTanggalLahir.getText().toString(),
                jeniskelamin, etNoHp.getText().toString(),3).enqueue(new Callback<ResponseBody>(){
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) {
                    loading.dismiss();
                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("status").equals("success")) {
                            // Jika register berhasil
                            String error_message = jsonRESULTS.getString("message");
                            Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else {
                            // Jika register gagal
                            String error_message = jsonRESULTS.getString("message");
                            Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
                else {
                    loading.dismiss();
//                    AlertDialog.Builder alert = new AlertDialog.Builder(getApplicationContext());
//                    alert.setTitle("Cek Koneksi Internet Anda");
//                    // alert.setMessage("Message");
//
//                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int whichButton) {
//                            //Your action here
//                        }
//                    });
//                    alert.show();

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("debug", "onFailure: ERROR > " + t.toString());
                loading.dismiss();
            }
        });

        }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etTanggalLahir.setText(sdf.format(calendar.getTime()));

    }

}
